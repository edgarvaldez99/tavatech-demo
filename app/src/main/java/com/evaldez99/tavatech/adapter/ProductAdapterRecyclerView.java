package com.evaldez99.tavatech.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evaldez99.tavatech.R;
import com.evaldez99.tavatech.model.Product;
import com.evaldez99.tavatech.view.ProductDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by evaldez on 14/04/17.
 */
public class ProductAdapterRecyclerView extends RecyclerView.Adapter<ProductAdapterRecyclerView.ProductViewHolder> {

    private ArrayList<Product> products;
    private int resource;
    private Activity activity;

    public ProductAdapterRecyclerView(ArrayList<Product> products, int resource, Activity activity) {
        this.products = products;
        this.resource = resource;
        this.activity = activity;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product product = products.get(position);
        holder.nameCard.setText(product.getName());
        holder.descriptionCard.setText(product.getDescription());
        holder.likeNumberCard.setText(product.getLikeNumber());
        Picasso.with(activity).load(product.getImageURL()).into(holder.productCard);

        holder.productCard.setOnClickListener((View view) -> {
            Intent intent = new Intent(activity, ProductDetailActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                Explode explode = new Explode();
                explode.setDuration(1000);
                activity.getWindow().setExitTransition(explode);
                activity.startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity, view, activity.getString(R.string.transitionname_product)).toBundle());
            }else {
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void clear() {
        int size = this.products.size();
        this.products.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.imageCard) ImageView productCard;
        @BindView(R.id.nameCard) TextView nameCard;
        @BindView(R.id.descriptionCard) TextView descriptionCard;
        @BindView(R.id.likeNumberCard) TextView likeNumberCard;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}