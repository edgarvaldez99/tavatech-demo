package com.evaldez99.tavatech.helper;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.evaldez99.tavatech.R;

/**
 * Created by evaldez on 15/04/17.
 */
public class Helper {

    public static void showToolbar(AppCompatActivity activity, String tittle, boolean upButton){
        showToolbar(activity, null, tittle, upButton);
    }

    public static void showToolbar(AppCompatActivity activity, View view, String tittle, boolean upButton){
        Toolbar toolbar;
        if(view == null){
            toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        }else{
            toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        }
        if(toolbar != null) {
            activity.setSupportActionBar(toolbar);
            ActionBar actionBar = activity.getSupportActionBar();
            actionBar.setTitle(tittle);
            actionBar.setDisplayHomeAsUpEnabled(upButton);
        }
    }
}