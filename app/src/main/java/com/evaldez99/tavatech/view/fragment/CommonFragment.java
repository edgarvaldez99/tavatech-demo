package com.evaldez99.tavatech.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evaldez99.tavatech.R;
import com.evaldez99.tavatech.adapter.ProductAdapterRecyclerView;
import com.evaldez99.tavatech.helper.Helper;
import com.evaldez99.tavatech.model.Product;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by evaldez on 17/04/17.
 */
public abstract class CommonFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        Helper.showToolbar((AppCompatActivity) getActivity(), view, getToolbarTitle(), false);
        RecyclerView productsRecycler = (RecyclerView) view.findViewById(getRecycleViewResource());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        productsRecycler.setLayoutManager(linearLayoutManager);
        ProductAdapterRecyclerView productAdapterRecyclerView = new ProductAdapterRecyclerView(buidProducts(), R.layout.cardview_product, getActivity());
        productsRecycler.setAdapter(productAdapterRecyclerView);
        return view;
    }

    protected ArrayList<Product> buidProducts(){
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Product("http://www.novalandtours.com/images/guide/guilin.jpg", "Producto 1", "Descripción producto 1", "3 Me Gusta"));
        products.add(new Product("http://www.enjoyart.com/library/landscapes/tuscanlandscapes/large/Tuscan-Bridge--by-Art-Fronckowiak-.jpg", "Producto 2", "Descripción producto 2", "10 Me Gusta"));
        products.add(new Product("http://www.educationquizzes.com/library/KS3-Geography/river-1-1.jpg", "Producto 3", "Descripción producto 3", "9 Me Gusta"));
        return products;
    }

    protected String getToolbarTitle(){
        return "";
    }

    protected abstract int getLayoutResource();

    protected int getRecycleViewResource(){
        return R.id.productRecycler;
    }
}