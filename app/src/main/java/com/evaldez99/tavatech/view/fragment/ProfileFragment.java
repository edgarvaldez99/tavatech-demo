package com.evaldez99.tavatech.view.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evaldez99.tavatech.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends CommonFragment {

    @Override
    protected int getLayoutResource(){
        return R.layout.fragment_profile;
    }

    @Override
    protected int getRecycleViewResource() {
        return R.id.productProfileRecycler;
    }
}
