package com.evaldez99.tavatech.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.evaldez99.tavatech.R;
import com.evaldez99.tavatech.view.fragment.HomeFragment;
import com.evaldez99.tavatech.view.fragment.ProfileFragment;
import com.evaldez99.tavatech.view.fragment.SearchFragment;
import com.roughike.bottombar.BottomBar;

public class ContainerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);BottomBar bottomBar = (BottomBar) findViewById(R.id.bottombar);
        bottomBar.setDefaultTab(R.id.home);
        bottomBar.setOnTabSelectListener((tabId)->{
            switch (tabId){
                case R.id.home:
                    addFragment(new HomeFragment());
                    break;
                case R.id.profile:
                    addFragment(new ProfileFragment());
                    break;
                case R.id.search:
                    addFragment(new SearchFragment());
                    break;
            }
        });
    }

    private void addFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null).commit();
    }
}