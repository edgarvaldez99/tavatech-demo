package com.evaldez99.tavatech.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evaldez99.tavatech.R;
import com.evaldez99.tavatech.adapter.ProductAdapterRecyclerView;
import com.evaldez99.tavatech.helper.Helper;
import com.evaldez99.tavatech.model.Product;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends CommonFragment {

    @Override
    protected int getLayoutResource(){
        return R.layout.fragment_home;
    }

    @Override
    protected String getToolbarTitle() {
        return getResources().getString(R.string.tab_home);
    }

}