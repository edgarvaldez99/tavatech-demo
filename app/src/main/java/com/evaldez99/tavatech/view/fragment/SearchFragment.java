package com.evaldez99.tavatech.view.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evaldez99.tavatech.R;
import com.evaldez99.tavatech.adapter.ProductAdapterRecyclerView;
import com.evaldez99.tavatech.model.Product;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class SearchFragment extends CommonFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        SearchView searchView = (SearchView) view.findViewById(R.id.search);
        searchView.setQueryHint(getActivity().getString(R.string.search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    RecyclerView productsRecycler = (RecyclerView) view.findViewById(R.id.productRecycler);
                    ProductAdapterRecyclerView adapter = (ProductAdapterRecyclerView) productsRecycler.getAdapter();
                    adapter.clear();
                    ArrayList<Product> list = buidProducts(), products = new ArrayList<Product>();
                    for(Product product:list){
                        if(product.getName().toLowerCase().contains(newText.toLowerCase())){
                            products.add(product);
                        }
                    }
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    productsRecycler.setLayoutManager(linearLayoutManager);
                    ProductAdapterRecyclerView productAdapterRecyclerView = new ProductAdapterRecyclerView(products, R.layout.cardview_product, getActivity());
                    productsRecycler.setAdapter(productAdapterRecyclerView);
                    adapter.notifyDataSetChanged();
                    //adapter.notifyDataSetInvalidated();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                return false;
            }
        });
        return view;
    }

    @Override
    protected int getLayoutResource(){
        return R.layout.fragment_search;
    }
}