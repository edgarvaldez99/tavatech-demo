package com.evaldez99.tavatech.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.evaldez99.tavatech.R;
import com.evaldez99.tavatech.helper.Helper;

public class CreateAccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        Helper.showToolbar(this, getResources().getString(R.string.toolbar_tittle_createaccount), true);
    }
}