package com.evaldez99.tavatech.model;

/**
 * Created by evaldez on 4/04/17.
 */
public class Product {
    private String imageURL;
    private String name;
    private String description;
    private String likeNumber = "0";

    public Product(String imageURL, String name, String description, String likeNumber) {
        this.imageURL = imageURL;
        this.name = name;
        this.description = description;
        this.likeNumber = likeNumber;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLikeNumber() {
        return likeNumber;
    }

    public void setLikeNumber(String likeNumber) {
        this.likeNumber = likeNumber;
    }
}